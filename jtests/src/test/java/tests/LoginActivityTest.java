package tests;

import com.example.ofrolov.sample.LoginActivity;
import com.example.ofrolov.sample.NewsActivity;
import com.example.ofrolov.sample.R;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.shadows.ShadowIntent;
import org.robolectric.util.ActivityController;

import android.content.Intent;
import android.view.View;

/**
 * Created by ofrolov on 09.09.2014.
 */
@RunWith(RobolectricGradleTestRunner.class)
public class LoginActivityTest {

    private LoginActivity activity;

    @Before
    public void setUp() {
        ActivityController controller = Robolectric.buildActivity(LoginActivity.class).create();
        activity = (LoginActivity)controller.get();
    }

    @Test
    public void activityNotNull() throws Exception {
        Assert.assertNotNull(activity);
    }

    @Test
    public void activityHasLoginForm() throws Exception {
        View login = activity.findViewById(R.id.editLogin);
        View password = activity.findViewById(R.id.editPassword);
        View button = activity.findViewById(R.id.button);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        Assert.assertNotNull(button);
    }

    @Test
    public void showNewsListShouldStartNewsActivity() throws Exception {
        activity.showNewsList();
        Intent intent = Robolectric.shadowOf(activity).getNextStartedActivity();
        Assert.assertNotNull(intent);
        ShadowIntent shadowIntent = Robolectric.shadowOf(intent);
        Assert.assertEquals(shadowIntent.getComponent().getClassName(), NewsActivity.class.getName());
    }
}
