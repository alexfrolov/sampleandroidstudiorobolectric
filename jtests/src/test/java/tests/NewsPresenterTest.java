package tests;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.rss.Channel;
import com.google.code.rome.android.repackaged.com.sun.syndication.feed.rss.Item;

import com.example.ofrolov.sample.presenter.NewsPresenter;
import com.example.ofrolov.sample.task.Task;
import com.example.ofrolov.sample.task.TaskCompleted;
import com.example.ofrolov.sample.view.NewsView;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by ofrolov on 10.09.2014.
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsPresenterTest {

    @Mock NewsView view;

    Channel result;
    boolean cancelled;

    {
        result = new Channel();
        List items = new ArrayList();
        Item item = new Item();
        item.setAuthor("author");
        items.add(item);
        result.setItems(items);
    }

    class StubLoadingTask implements Task<String> {

        private final TaskCompleted<Channel> listener;

        StubLoadingTask(TaskCompleted<Channel> listener) {
            this.listener = listener;
        }

        @Override
        public void start(String... params) {
            listener.onCompleted(result);
        }

        @Override
        public void cancel() {
            cancelled = true;
        }
    }

    class StubNewsPresenter extends NewsPresenter {

        public StubNewsPresenter(NewsView view) {
            super(view);
        }

        @Override
        protected Task<String> createLoadingTask() {
            return new StubLoadingTask(this);
        }
    }

    @Test
    public void shouldLoadDataTest() throws Exception {
        NewsPresenter presenter = new StubNewsPresenter(view);
        presenter.loadNews();
        verify(view, times(1)).show(result);
    }

    @Test
    public void cancelLoadingTest() {
        NewsPresenter presenter = new StubNewsPresenter(view);
        presenter.loadNews();
        presenter.cancelLoading();
        Assert.assertEquals(true, cancelled);
    }
}
