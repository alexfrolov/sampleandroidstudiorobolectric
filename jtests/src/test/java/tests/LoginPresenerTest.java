package tests;

import com.example.ofrolov.sample.presenter.LoginPresenter;
import com.example.ofrolov.sample.view.LoginView;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ofrolov on 09.09.2014.
 */
public class LoginPresenerTest implements LoginView {

    boolean error = false;

    @Override
    public void setErrorLogin(String message) {
        error = true;
    }

    @Override
    public void showNewsList() {
        error = false;
    }

    @Test
    public void loginTest() throws Exception {
        LoginPresenter presenter = new LoginPresenter(this);

        // test wrong credentials
        presenter.login("asd", "123");
        Assert.assertEquals("wrong credentials", error, true);

        // test wrong credentials
        presenter.login("user", "");
        Assert.assertEquals("wrong credentials", error, true);

        // test right credentials
        presenter.login("user", "123");
        Assert.assertEquals(false, error);

    }

}
