package com.example.ofrolov.sample.adapter;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.rss.Channel;
import com.google.code.rome.android.repackaged.com.sun.syndication.feed.rss.Item;

import com.example.ofrolov.sample.R;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by ofrolov on 10.09.2014.
 */
public class NewsAdapter extends BaseAdapter {

    private final List<Item> items;

    private final LayoutInflater inflater;

    public NewsAdapter(Context context, Channel channel) {
        items = (List <Item>)channel.getItems();
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Holder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.news_item, null);
            holder = new Holder(view);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }

        Item item = (Item) getItem(i);
        holder.txtTitle.setText(item.getTitle());
        holder.txtDescription.setText(item.getPubDate().toString());

        return view;
    }

    static class Holder {
        @InjectView(R.id.txtTitle)
        TextView txtTitle;

        @InjectView(R.id.txtDescription)
        TextView txtDescription;

        Holder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
