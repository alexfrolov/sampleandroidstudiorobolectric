package com.example.ofrolov.sample.model;

/**
 * Created by ofrolov on 09.09.2014.
 */
public class LoginModel {

    private String error;

    public boolean validate(String login, String password) {
        if ("user".equals(login) && "123".equals(password)) {
            return true;
        }
        error = "access denied";
        return false;
    }

    public String getError() {
        return error;
    }
}
