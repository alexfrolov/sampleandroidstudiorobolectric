package com.example.ofrolov.sample.task;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.rss.Channel;

import com.example.ofrolov.sample.service.NewsService;

import org.springframework.http.MediaType;
import org.springframework.http.converter.feed.RssChannelHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import android.os.AsyncTask;

import java.util.Collections;

public class LoadDataTask extends AsyncTask<String, Object, Channel> implements Task<String> {

    private final TaskCompleted<Channel> listener;

    private final NewsService service;

    public LoadDataTask(NewsService service, TaskCompleted<Channel> listener) {
        this.service = service;
        this.listener = listener;
    }

    @Override
    protected Channel doInBackground(String... params) {
        return service.loadNews();
    }

    @Override
    protected void onPostExecute(Channel result) {
        listener.onCompleted(result);
    }

    @Override
    public void start(String... params) {
        execute(params);
    }

    @Override
    public void cancel() {
        cancel(true);
    }
}