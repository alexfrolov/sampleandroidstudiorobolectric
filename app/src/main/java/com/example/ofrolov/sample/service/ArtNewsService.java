package com.example.ofrolov.sample.service;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.rss.Channel;

import org.springframework.http.MediaType;
import org.springframework.http.converter.feed.RssChannelHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

/**
 * Created by ofrolov on 10.09.2014.
 */
public class ArtNewsService implements NewsService {

    private static final String NEWS_URL = "http://feeds.reuters.com/news/artsculture";

    @Override
    public Channel loadNews() {
        RestTemplate restTemplate = new RestTemplate();
        RssChannelHttpMessageConverter rssChannelConverter = new
                RssChannelHttpMessageConverter();
        rssChannelConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_XML));
        restTemplate.getMessageConverters().add(rssChannelConverter);
        return restTemplate.getForObject(NEWS_URL, Channel.class);
    }
}
