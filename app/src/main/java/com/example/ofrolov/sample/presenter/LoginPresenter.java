package com.example.ofrolov.sample.presenter;

import com.example.ofrolov.sample.model.LoginModel;
import com.example.ofrolov.sample.view.LoginView;

/**
 * Created by ofrolov on 09.09.2014.
 */
public class LoginPresenter {

    private final LoginView view;
    private final LoginModel model;

    public LoginPresenter(LoginView view) {
        this.view = view;
        this.model = new LoginModel();
    }

    public void login(String login, String password) {
        if (model.validate(login, password)) {
            view.showNewsList();
        } else {
            view.setErrorLogin(model.getError());
        }
    }

}
