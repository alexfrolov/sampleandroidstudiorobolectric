package com.example.ofrolov.sample.service;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.rss.Channel;

/**
 * Created by ofrolov on 10.09.2014.
 */
public interface NewsService {

    Channel loadNews();
}
