package com.example.ofrolov.sample.task;

/**
 * Created by ofrolov on 10.09.2014.
 */
public interface TaskCompleted<T> {

    void onCompleted(T result);
}
