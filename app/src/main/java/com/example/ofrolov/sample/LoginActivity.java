package com.example.ofrolov.sample;

import com.example.ofrolov.sample.presenter.LoginPresenter;
import com.example.ofrolov.sample.view.LoginView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class LoginActivity extends Activity implements LoginView {

    @InjectView(R.id.editLogin)
    EditText editLogin;

    @InjectView(R.id.editPassword)
    EditText editPassword;

    @InjectView(R.id.txtError)
    TextView txtError;

    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        ButterKnife.inject(this, this);
        presenter = new LoginPresenter(this);
    }

    @OnClick(R.id.button)
    public void loginButtonClick() {
        presenter.login(editLogin.getText().toString(), editPassword.getText().toString());
    }

    @Override
    public void setErrorLogin(String message) {
        txtError.setText(message);
        txtError.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNewsList() {
        startActivity(new Intent(this, NewsActivity.class));
        finish();
    }
}
