package com.example.ofrolov.sample;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.rss.Channel;

import com.example.ofrolov.sample.adapter.NewsAdapter;
import com.example.ofrolov.sample.presenter.NewsPresenter;
import com.example.ofrolov.sample.view.NewsView;

import android.app.ListActivity;
import android.os.Bundle;

/**
 * Created by ofrolov on 09.09.2014.
 */
public class NewsActivity extends ListActivity implements NewsView {

    private NewsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new NewsPresenter(this);
    }


    @Override
    public void show(Channel channel) {
        setListAdapter(new NewsAdapter(this, channel));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.loadNews();
    }

    @Override
    protected void onPause() {
        presenter.cancelLoading();
        super.onPause();
    }
}
