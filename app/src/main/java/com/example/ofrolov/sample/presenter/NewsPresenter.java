package com.example.ofrolov.sample.presenter;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.rss.Channel;

import com.example.ofrolov.sample.service.ArtNewsService;
import com.example.ofrolov.sample.task.LoadDataTask;
import com.example.ofrolov.sample.task.Task;
import com.example.ofrolov.sample.task.TaskCompleted;
import com.example.ofrolov.sample.view.NewsView;

/**
 * Created by ofrolov on 10.09.2014.
 */
public class NewsPresenter implements TaskCompleted<Channel> {

    private final NewsView view;

    private Task<String> task;

    public NewsPresenter(NewsView view) {
        this.view = view;
    }

    protected Task<String> createLoadingTask() {
        return new LoadDataTask(new ArtNewsService(), this);
    }

    public void loadNews() {
        task = createLoadingTask();
        task.start();
    }

    public void cancelLoading() {
        if (task != null) {
            task.cancel();
        }
    }

    @Override
    public void onCompleted(Channel result) {
        view.show(result);
    }
}
